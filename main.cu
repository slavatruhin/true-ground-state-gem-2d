#include <iostream>
#include <fstream>
#include <vector>


#define CUDA_CHECK_ERROR(err)           \
if ((err) != cudaSuccess)               \
{          \
    printf("Cuda error: %s\n", cudaGetErrorString(err));    \
    printf("Error in file: %s, line: %i\n", __FILE__, __LINE__);  \
}
#define n 6 // длина одномерной цепочки
#define J 1 // обменный интегралл

struct CHAIN
{
    __int8 S[n]{};
};

struct GEM
{
    unsigned __int32 G=0;
    __int16 E=0;
    __int16 M=0;
    unsigned __int32 conf[1<<10]{}; //последние комбинации с текущим вырождением
};

struct shared_GEM
{
    unsigned __int32 G;
    __int16 E;
    __int16 M;
    unsigned __int32 conf;
};

struct block_GEM
{
    unsigned __int32 G=0;
    __int16 E=0;
    __int16 M=0;
    unsigned __int32 conf=0;
};

__global__ void ThreadInit(CHAIN *chain, GEM *gem_o, GEM *gem_even, unsigned int length)
{
    unsigned int x = threadIdx.x;
    unsigned int spread_lim = length/32;
    if (x<32)
    {
        unsigned __int32 bit;
        unsigned __int32 thread;
        for (int spread = 0; spread < spread_lim; spread++)
        {
            thread = x + spread * 32;
            bit = thread;
            __shared__ shared_GEM gem_part[32];
            gem_part[x].E = 0;
            gem_part[x].M = 0;
            gem_part[x].conf = 0;
            for (auto & i : chain[thread].S)
            {
                i = bit & 1 ? 1 : -1;
                bit >>= 1;
                gem_part[x].M += i;
            }
            gem_part[x].conf = thread;
            for (int i=0; i<n; i++)
            {
                gem_part[x].E += -J*chain[thread].S[i]*chain[thread].S[(i+1)%n];
            }
            __syncthreads();
            if (x == 0)
            {
                for (auto & j : gem_part)
                {
                    for (int i=0; i<length; i++)
                    {
                        if (gem_o[i].E == j.E && gem_o[i].M == j.M)
                        {
                            gem_o[i].G++;
                            gem_even[i].G++;
                            for (int k=0; k<(1<<10);k++)
                            {
                                if (gem_o[i].conf[k]<1)
                                {
                                    gem_o[i].conf[k] = j.conf;
                                    gem_even[i].conf[k] = j.conf;
                                    break;
                                }
                            }
                            break;
                        }
                        if (gem_o[i].G < 1)
                        {
                            gem_o[i].M = j.M;
                            gem_even[i].M = j.M;
                            gem_o[i].E = j.E;
                            gem_even[i].E = j.E;
                            gem_o[i].G++;
                            gem_even[i].G++;
                            gem_o[i].conf[0]=j.conf;
                            gem_even[i].conf[0]=j.conf;
                            break;
                        }
                    }
                }
            }
            __syncthreads();
        }
    }
}

__global__ void Gem(CHAIN *chain, GEM *gem_o, GEM *gem_even, block_GEM *gem_block, const int *l, const int *r,
                    const int *conf_spread, const int *conf_r, bool *spread_bool, bool *r_bool)
{
    auto x = threadIdx.x + blockIdx.x*blockDim.x;
    __shared__ shared_GEM emc[512];
    emc[threadIdx.x].E = 0;
    emc[threadIdx.x].M = 0;
    emc[threadIdx.x].G = 0;
    emc[threadIdx.x].conf = 0;
    if (gem_even[*l].conf[x+*conf_spread*x]>0 && gem_o[*r].conf[*conf_r]>0)
    {
        int E = 0;
        for (int j = 0; j < n; j++)
        {
            E += J * chain[gem_even[*l].conf[x + *conf_spread * x]].S[j] * chain[gem_o[*r].conf[*conf_r]].S[j];
        }
        emc[threadIdx.x].M = gem_even[*l].M + gem_o[*r].M;
        emc[threadIdx.x].E = gem_even[*l].E + gem_o[*r].E + E;
        emc[threadIdx.x].G = 1;
        __syncthreads();
        if (threadIdx.x == 0)
        {
            for (int i = 0; i < blockDim.x; i++)
            {
                if (emc[i].G > 0)
                {
                    gem_block[i + blockIdx.x * blockDim.x].E = emc[i].E;
                    gem_block[i + blockIdx.x * blockDim.x].M = emc[i].M;
                    gem_block[i + blockIdx.x * blockDim.x].G = emc[i].G;
                    gem_block[i + blockIdx.x * blockDim.x].conf = emc[i].conf;
                }
            }
        }
    }
    __syncthreads();
    if (gem_even[*l].conf[x+*conf_spread*x]<1)
    {
        *spread_bool = true;
    }
    if (gem_o[*r].conf[*conf_r+1]<1)
    {
        *r_bool = true;
    }
}

__global__ void Sort(block_GEM *gem_block, GEM *gem_o, GEM * gem_even, GEM *gem_odd,
                     int *l, int *r, int *conf_spread,  int *conf_r, const bool *spread_bool, const bool *r_bool)
{
    if(!*r_bool)
    {
        *conf_r = *conf_r + 1;
    }
    else
    {
        *conf_r = 0;
        if(!*spread_bool)
        {
            *conf_spread = *conf_spread + 1;
        }
        else
        {
            *conf_spread = 0;
            if(gem_o[*r+1].G>0)
            {
                *r = *r + 1;
                *conf_spread = 0;
            }
            else
            {
                if(gem_even[*l+1].G>0)
                {
                    *l = *l + 1;
                    *r = 0;
                    *conf_spread = 0;
                }
                else
                {
                    *l = -1;
                    *r = -1;
                    *conf_spread = 0;
                }
            }
        }
    }
    // TODO: Записть в результата
    //for (int i=0; i<1024; i++)
    //{
    //    for (int j=0; j<1024; j++)
    //    {
    //        if (gem_block[i].E == gem_odd[j].E && gem_block[i].M == gem_odd[j].M)
    //        {
    //            gem_odd[j].G++;
    //        }
    //        if (gem_odd[j].G<1)
    //        {
    //            gem_odd[j].E = gem_block[i].E;
    //            gem_odd[j].M = gem_block[i].M;
    //            gem_odd[j].G = 1;
    //        }
    //    }
    //}
}

int main()
{
    unsigned int length = 1<<n;
    std::vector<CHAIN> chain(length);
    std::vector<GEM> gem_o(length);
    std::vector<GEM> gem_odd(length);
    std::vector<GEM> gem_even(length);
    CHAIN *dev_chain;
    GEM *dev_gem_o, *dev_gem_even, *dev_gem_odd;
    block_GEM *dev_gem_block;
    CUDA_CHECK_ERROR(cudaMalloc((void**)&dev_chain, length*sizeof (CHAIN)))
    CUDA_CHECK_ERROR(cudaMalloc((void**)&dev_gem_o, length*sizeof (GEM)))
    CUDA_CHECK_ERROR(cudaMalloc((void**)&dev_gem_odd, length*sizeof (GEM)))
    CUDA_CHECK_ERROR(cudaMalloc((void**)&dev_gem_even, length*sizeof (GEM)))
    CUDA_CHECK_ERROR(cudaMalloc((void**)&dev_gem_block, (1<<19)*sizeof (block_GEM)))
    CUDA_CHECK_ERROR(cudaMemcpy(dev_chain, chain.data(), length*sizeof (CHAIN), cudaMemcpyHostToDevice))
    CUDA_CHECK_ERROR(cudaMemcpy(dev_gem_o, gem_o.data(), length*sizeof (GEM), cudaMemcpyHostToDevice))
    CUDA_CHECK_ERROR(cudaMemcpy(dev_gem_odd, gem_odd.data(), length*sizeof (GEM), cudaMemcpyHostToDevice))
    CUDA_CHECK_ERROR(cudaMemcpy(dev_gem_even, gem_even.data(), length*sizeof (GEM), cudaMemcpyHostToDevice))
    ThreadInit<<<1, 32>>>(dev_chain, dev_gem_o, dev_gem_even, length);
    int l=0, r=0, conf_spread=0, conf_r=0;
    bool spread_bool = false, r_bool = false;
    int *dev_l=nullptr, *dev_r=nullptr, *dev_conf_spread=nullptr, *dev_conf_r=nullptr;
    bool *dev_spread_bool = nullptr, *dev_r_bool = nullptr;
    CUDA_CHECK_ERROR(cudaMalloc((void**)&dev_l, sizeof(int)))
    CUDA_CHECK_ERROR(cudaMalloc((void**)&dev_r, sizeof(int)))
    CUDA_CHECK_ERROR(cudaMalloc((void**)&dev_conf_spread, sizeof(int)))
    CUDA_CHECK_ERROR(cudaMalloc((void**)&dev_conf_r, sizeof(int)))
    CUDA_CHECK_ERROR(cudaMalloc((void**)&dev_spread_bool, sizeof(bool)))
    CUDA_CHECK_ERROR(cudaMalloc((void**)&dev_r_bool, sizeof(bool)))
    while (l>-1 && r>-1)
    {
        printf("before GPU l = %d, r = %d; ", l, r);
        while (conf_spread>-1 && conf_r>-1)
        {
            CUDA_CHECK_ERROR(cudaMemcpy(dev_spread_bool, &spread_bool, sizeof(bool), cudaMemcpyHostToDevice))
            CUDA_CHECK_ERROR(cudaMemcpy(dev_r_bool, &r_bool, sizeof(bool), cudaMemcpyHostToDevice))
            CUDA_CHECK_ERROR(cudaMemcpy(dev_conf_spread, &conf_spread, sizeof(int), cudaMemcpyHostToDevice))
            CUDA_CHECK_ERROR(cudaMemcpy(dev_conf_r, &conf_r, sizeof(int), cudaMemcpyHostToDevice))
            CUDA_CHECK_ERROR(cudaMemcpy(dev_l, &l, sizeof(int), cudaMemcpyHostToDevice))
            CUDA_CHECK_ERROR(cudaMemcpy(dev_l, &l, sizeof(int), cudaMemcpyHostToDevice))
            Gem<<<1024, 512>>>(dev_chain, dev_gem_o, dev_gem_even, dev_gem_block,
                                               dev_l, dev_r, dev_conf_spread, dev_conf_r,
                                               dev_spread_bool, dev_r_bool);
            Sort<<<1,1>>>(dev_gem_block, dev_gem_o, dev_gem_even, dev_gem_odd,
                                          dev_l, dev_r, dev_conf_spread, dev_conf_r,
                                          dev_spread_bool, dev_r_bool);
            CUDA_CHECK_ERROR(cudaMemcpy(&l, dev_l, sizeof(int), cudaMemcpyDeviceToHost))
            CUDA_CHECK_ERROR(cudaMemcpy(&r, dev_r, sizeof(int), cudaMemcpyDeviceToHost))
            CUDA_CHECK_ERROR(cudaMemcpy(&conf_spread, dev_conf_spread, sizeof(int), cudaMemcpyDeviceToHost))
            CUDA_CHECK_ERROR(cudaMemcpy(&conf_r, dev_conf_r, sizeof(int), cudaMemcpyDeviceToHost))
            CUDA_CHECK_ERROR(cudaMemcpy(&spread_bool, dev_spread_bool, sizeof(bool), cudaMemcpyDeviceToHost))
            CUDA_CHECK_ERROR(cudaMemcpy(&r_bool, dev_r_bool, sizeof(bool), cudaMemcpyDeviceToHost))
        }
        printf("after GPU l = %d, r = %d \n", l, r);
    }
    CUDA_CHECK_ERROR(cudaMemcpy(gem_o.data(), dev_gem_o, length*sizeof (GEM), cudaMemcpyDeviceToHost))
    CUDA_CHECK_ERROR(cudaMemcpy(gem_even.data(), dev_gem_even, length*sizeof (GEM), cudaMemcpyDeviceToHost))
    CUDA_CHECK_ERROR(cudaFree(dev_chain))
    CUDA_CHECK_ERROR(cudaFree(dev_gem_o))
    CUDA_CHECK_ERROR(cudaFree(dev_gem_odd))
    CUDA_CHECK_ERROR(cudaFree(dev_gem_even))
    CUDA_CHECK_ERROR(cudaFree(dev_l))
    CUDA_CHECK_ERROR(cudaFree(dev_r))
    int term = 0;
    std::ofstream file("Gem.txt");
    for (int i=0; i<length; i++)
    {
        if (gem_even[i].G>0)
        {
            std::cout << "G=" << gem_even[i].G << " E=" << gem_even[i].E << "  M=" << gem_even[i].M << " Configurations:";
            for (int j=0; j<(1<<7); j++)
            {
                if (gem_even[i].conf[j] == 0) break;
                std::cout << " " << gem_even[i].conf[j];
            }
            std::cout <<"\n";
            file << "G=" << gem_even[i].G << " E=" << gem_even[i].E << "  M=" << gem_even[i].M << " Configurations:";
            for (int j=0; j<(1<<7); j++)
            {
                if (gem_even[i].conf[j] == 0) break;
                file << " " << gem_even[i].conf[j];
            }
            file <<"\n";
            term++;
        }
    }
    std::cout << "Terms: " << term << "\n";
    file << "Terms: " << term << "\n";
}